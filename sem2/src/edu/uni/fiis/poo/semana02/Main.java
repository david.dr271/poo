package edu.uni.fiis.poo.semana02;

public class Main{

    public static void main(String[] args){
        ArrayList<Persona> lista = new ArrayList<>();
        Persona a = new Alumno( nombre:"Juan", apellido:"Perez", edad: 21, ciclo: 2, promedio: 12.5f);
        Persona p1 = new Persona( nombre: "Ana", apellido: "Alvarez", edad: 16);

        lista.add(a);
        lista.add(p1);

        for(Persona per: lista){
           if(per.getEdad()>20){
               per.mostrarDatos();
           }
        }
    }
}
