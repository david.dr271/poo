package edu.uni.fiis.poo.semana02;

public class Alumno extends Persona {
    int ciclo;
    float promedio;

    public Alumno(String nombre, String apellido, int edad, int ciclo, float promedio){
        super(nombre, apellido, edad);
        this.ciclo = ciclo;
        this.promedio = promedio;
    }
    public void mostrarDatos(){
        super.mostrarDatos();
        System.out.println(" Ciclo: " + ciclo);
        System.out.println(" Ponderado: " + promedio);
    }
}
