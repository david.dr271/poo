package edu.uni.fiis.poo.semana02;

public class Persona {
    private String nombre;
    private String apellido;

    public int getEdad() {
        return edad;
    }


    private int edad;

    public Persona(String nombre, String apellido, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    public String obtenerNombreCompleto(){
        return nombre + " " + apellido;
    }
    public void mostrarDatos(){
        System.out.println("Nombre: " + obtenerNombreCompleto());
        System.out.println(" Edad: " + edad);
    }
    public void incrementarEdad(){
        edad++;
    }
}